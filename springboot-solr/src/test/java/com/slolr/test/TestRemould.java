package com.slolr.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.solr.SpringbootApplication;
import com.solr.helper.entity.FieldAttrObj;
import com.solr.helper.exception.SolrHelperException;
import com.solr.helper.page.SolrPageHelper;
import com.solr.helper.proxy.SelectCustom;
import com.solr.helper.service.SolrHelper;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes={SpringbootApplication.class})
public class TestRemould {
	
	private Logger logger = LoggerFactory.getLogger(TestRemould.class);
	
	@Autowired
	private SolrHelper solrHelper;
	
	@Test//1、创建域
	public void createField() throws SolrHelperException {
		Boolean data = solrHelper.createField("demo", new FieldAttrObj("column0","string")).getData();
		logger.info("执行结果：{}",data);
	}
	
	@Test//2、修改域类型建域
	public void updateFieldType() throws SolrHelperException {
		Boolean data = null;
		try {
			data = solrHelper.replaceField("demo", new FieldAttrObj("column0", "long")).getData();
		} catch (SolrServerException | IOException e) {
			e.printStackTrace();
		}
		logger.info("执行结果：{}",data);
	}
	
	@Test//3、删除域
	public void removeField(){
		Boolean data = solrHelper.removeField("demo", "column0").getData();
		logger.info("执行结果：{}",data);
	}
	
	@Test//4、判断域是否存在
	public void existField(){
		Boolean data = solrHelper.existField("demo", "column1").getData();
		logger.info("执行结果：{}",data);
	}
	
	
	@Test//5、查询单个域
	public void getField(){
		FieldAttrObj data = solrHelper.getField("demo", "column1").getData();
		logger.info("执行结果：{}",data);
	}
	
	@Test//6、查询所有域
	public void getFields(){
		Map<String, FieldAttrObj> fields = solrHelper.getFields("demo").getData();
		for(Map.Entry<String, FieldAttrObj> entry : fields.entrySet()){
			logger.info("执行结果：key={} , value={}", entry.getKey(), entry.getValue());
		}
	}
	
	
	@Test//7、增量添加一条数据
	public void updateIncrement() throws SolrHelperException {
		Map<String,Object> map = new HashMap<>();
		map.put("id", "1");
		map.put("column1", data);
		map.put("column2", data);
		map.put("column3", data);
		map.put("column4", data);
		map.put("column5", data);
		Boolean data = solrHelper.updateIncrement("demo", map).getData();
		logger.info("执行结果：{}",data);
	}
	
	@Test//8、全量添加一条数据
	public void updateFullDose() throws SolrHelperException {
		Map<String,Object> map = new HashMap<>();
		map.put("id", "12");
		map.put("column1", data);
		map.put("column2", data);
		map.put("column3", data);
		map.put("column4", data);
		map.put("column5", data);
		Boolean data = solrHelper.updateFullDose("demo", map).getData();
		logger.info("执行结果：{}",data);
	}
	
	@Test//9、全量批量添加数据，添加多条
	public void updateBatch() throws SolrHelperException {
		List<Map<String,Object>> list = new ArrayList<>();
		for(int i=0 ; i<10 ; i++) {
			Map<String,Object> map = new HashMap<>();
			map.put("id", i);
			map.put("column1", i);
			map.put("column2", i);
			map.put("column3", i);
			map.put("column4", i);
			map.put("column5", i);
			list.add(map);
		}
		Boolean data = solrHelper.updateBatch("demo", list).getData();
		logger.info("执行结果：{}",data);
	}
	
	@Test//10、批量全量添加数据，添加一条数据，单机模式
	public void updateBatchToStandalone1() throws SolrHelperException, SolrServerException, IOException {
		Map<String,Object> map = new HashMap<>();
		map.put("id", data);
		map.put("column1", data);
		map.put("column2", data);
		map.put("column3", data);
		map.put("column4", data);
		map.put("column5", data);
		Boolean data = solrHelper.updateBatchToStandalone("demo", map).getData();
		logger.info("执行结果：{}",data);
	}
	
	//未验证
	@Test//11、批量全量添加数据，添加多条数据，单机模式
	public void updateBatchToStandalone2() throws SolrHelperException, SolrServerException, IOException {
		List<Map<String,Object>> list = new ArrayList<>();
		for(int i=11 ; i<21 ; i++) {
			Map<String,Object> map = new HashMap<>();
			map.put("id", i);
			map.put("column1", i);
			map.put("column2", i);
			map.put("column3", i);
			map.put("column4", i);
			map.put("column5", i);
			list.add(map);
		}
		Boolean data = solrHelper.updateBatchToStandalone("demo", list).getData();
		logger.info("执行结果：{}",data);
	}
	
	
	
	@Test//12、根据一个id删除数据
	public void deleteDataById(){
		Boolean data = null;
		try {
			data = solrHelper.deleteDataById("demo", "11").getData();
		} catch (SolrServerException | IOException e) {
			e.printStackTrace();
		}
		logger.info("执行结果：{}",data);
	}
	
	@Test//13、根据多个id删除数据
	public void deleteDataByIds(){
		List<String> list = new ArrayList<>();
		list.add("0");
		list.add("1");
		list.add("2");
		Boolean data = null;
		try {
			data = solrHelper.deleteDataByIds("demo", list).getData();
		} catch (SolrServerException | IOException e) {
			e.printStackTrace();
		}
		logger.info("执行结果：{}",data);
	}
	
	@Test//14、查询数据
	public void queryForListPage() throws SolrServerException, IOException {
		SelectCustom sc = new SelectCustom() {
			@Override
			public SolrQuery select() {
				SolrQuery q = new SolrQuery("*:*");
				return q;
			}
		};
		SolrPageHelper data = solrHelper.queryForListPage("demo", new SolrPageHelper(1, 10), sc).getData();
		List<Map<String,Object>> data2 = data.getData();
		for (Map<String, Object> map : data2) {
			System.out.println(map);
		}
	}
	String data = "11";
}
