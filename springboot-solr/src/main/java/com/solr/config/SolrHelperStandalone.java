package com.solr.config;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.ConcurrentUpdateSolrClient;
import org.apache.solr.client.solrj.impl.ConcurrentUpdateSolrClient.Builder;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.springframework.data.solr.core.SolrTemplate;

public class SolrHelperStandalone {
	private String baseUrl;
	private int threadCount;
	private SolrClient solrClient;
	private SolrTemplate solrTemplate;
	private ConcurrentUpdateSolrClient concurrentUpdateSolrClient;
	
	public SolrHelperStandalone(String baseUrl, int threadCount){
		this.baseUrl = baseUrl;
		this.threadCount = threadCount;
	}

	public SolrHelperStandalone build() {
		setSolrClient(baseUrl);//创建solrClient
		setSolrTemplate(getSolrClient());//创建solrTemplate
		setConcurrentUpdateSolrClient(baseUrl);//创建solr批处理
		return this;
	}
	
	//原生客户端
	private void setSolrClient(String baseUrl) {
		this.solrClient = new HttpSolrClient.Builder(baseUrl).build();
	}
	
	public SolrClient getSolrClient() {
		return solrClient;
	}
	
	//spring管理的solr客户端
	private void setSolrTemplate(SolrClient solrClient) {
		this.solrTemplate = new SolrTemplate(solrClient);
	}
	public SolrTemplate getSolrTemplate() {
		return solrTemplate;
	}
	
	
	//单机批处理，仅限单机
	private void setConcurrentUpdateSolrClient(String baseUrl) {
		Builder builder = new ConcurrentUpdateSolrClient.Builder(baseUrl);
		builder.withThreadCount(threadCount);
		this.concurrentUpdateSolrClient = builder.build();
	}
	//solr单机批处理客户端
	public ConcurrentUpdateSolrClient getConcurrentUpdateSolrClient(){
		return concurrentUpdateSolrClient;
	}
}
