package com.solr.config;

import java.util.Arrays;
import java.util.Optional;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.impl.CloudSolrClient.Builder;
import org.springframework.data.solr.core.SolrTemplate;

public class SolrHelperCluster {
	private String zkHosts;
	private String zkNode;
	private int socketTimeout;
	private int connectionTimeout;
	
	private SolrClient solrClient;
	private SolrTemplate solrTemplate;
	
	public SolrHelperCluster(String zkHosts, String zkNode, int socketTimeout, int connectionTimeout) {
		this.zkHosts = zkHosts;
		this.zkNode = zkNode;
		this.socketTimeout = socketTimeout;
		this.connectionTimeout = connectionTimeout;
	}
	
	public SolrHelperCluster build() {
		setSolrClient(zkHosts, zkNode, socketTimeout, connectionTimeout);
		setSolrTemplate(solrClient);
		return this;
	}

	public SolrClient getSolrClient() {
		return solrClient;
	}

	public void setSolrClient(String zkHosts, String zkNode, int socketTimeout ,int connectionTimeout) {
		Optional<String> zkChroot = Optional.of(zkNode);//zookeeper指定节点
		Builder builder = new CloudSolrClient.Builder(Arrays.asList(zkHosts.split(",")), zkChroot)
				.withSocketTimeout(socketTimeout).withConnectionTimeout(connectionTimeout);
		this.solrClient = builder.build();
	}

	public SolrTemplate getSolrTemplate() {
		return solrTemplate;
	}

	public void setSolrTemplate(SolrClient solrClient) {
		this.solrTemplate = new SolrTemplate(solrClient);
	}
	
	
}
