package com.solr.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SolrHelperConfig {
	
	private Logger logger = LoggerFactory.getLogger(SolrHelperConfig.class);
	
	//单机配置 begin
	private String baseUrl;//solr地址
	private int threadCount;//单机使用的批处理线程数
	//单机配置 end
	
	//集群配置 begin
	private String zkHosts;
	private String zkNode;
	private int socketTimeout;
	private int connectionTimeout;
	//集群配置 end
	
	//公共配追 begin
	private long commitMillis;//软提交时间
	private String cluster;//是否以集群方式启动
	//公共配置 end
	
	private SolrHelperCluster solrHelperCluster;//创建solr集群
	private SolrHelperStandalone solrHelperStandalone;//创建solr单机
	
	public long getCommitMillis() {
		return commitMillis;
	}

	public void setCommitMillis(long commitMillis) {
		this.commitMillis = commitMillis;
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public int getThreadCount() {
		return threadCount;
	}

	public void setThreadCount(int threadCount) {
		this.threadCount = threadCount;
	}

	public void setCluster(String cluster) {
		if("true".equalsIgnoreCase(cluster)) {
			setSolrHelperCluster(new SolrHelperCluster(getZkHosts(), getZkNode(), getSocketTimeout(), getConnectionTimeout()).build());
		}else if("false".equalsIgnoreCase(cluster)){
			setSolrHelperStandalone(new SolrHelperStandalone(getBaseUrl(), getThreadCount()).build());
		}else {
			logger.info("是否以集群方式启动参数配参数错误：{}，true为集群，false为单机", cluster);
		}
		this.cluster = cluster;
	}
	
	
	public String getCluster() {
		return cluster;
	}

	public String getZkHosts() {
		return zkHosts;
	}

	public void setZkHosts(String zkHosts) {
		this.zkHosts = zkHosts;
	}

	public String getZkNode() {
		return zkNode;
	}

	public void setZkNode(String zkNode) {
		this.zkNode = zkNode;
	}

	public int getSocketTimeout() {
		return socketTimeout;
	}

	public void setSocketTimeout(int socketTimeout) {
		this.socketTimeout = socketTimeout;
	}

	public int getConnectionTimeout() {
		return connectionTimeout;
	}

	public void setConnectionTimeout(int connectionTimeout) {
		this.connectionTimeout = connectionTimeout;
	}

	public SolrHelperCluster getSolrHelperCluster() {
		return solrHelperCluster;
	}

	public void setSolrHelperCluster(SolrHelperCluster solrHelperCluster) {
		this.solrHelperCluster = solrHelperCluster;
	}

	public SolrHelperStandalone getSolrHelperStandalone() {
		return solrHelperStandalone;
	}

	public void setSolrHelperStandalone(SolrHelperStandalone solrHelperStandalone) {
		this.solrHelperStandalone = solrHelperStandalone;
	}

	
}
