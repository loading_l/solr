package com.solr.helper.entity;


public class FieldAttrObj {
	private String name;
	private String type;
	private boolean indexed = true;
	private boolean stored = true;
	private boolean multiValued = false;
	private boolean required = false;
	private Object defaultValue = null;
	
	
	public FieldAttrObj(String name, String type) {
		this.name = name.trim();
		this.type = type.trim();
	}
	
	public String getName() {
		return name;
	}
	public String getType() {
		return type;
	}
	public boolean isIndexed() {
		return indexed;
	}
	public FieldAttrObj setIndexed(boolean indexed) {
		this.indexed = indexed;
		return this;
	}
	public boolean isStored() {
		return stored;
	}
	public FieldAttrObj setStored(boolean stored) {
		this.stored = stored;
		return this;
	}
	public boolean isMultiValued() {
		return multiValued;
	}
	public FieldAttrObj setMultiValued(boolean multiValued) {
		this.multiValued = multiValued;
		return this;
	}
	public boolean isRequired() {
		return required;
	}
	public FieldAttrObj setRequired(boolean required) {
		this.required = required;
		return this;
	}
	public Object getDefaultValue() {
		return defaultValue;
	}
	public FieldAttrObj setDefaultValue(Object defaultValue) {
		this.defaultValue = defaultValue;
		return this;
	}
	@Override
	public String toString() {
		return "FieldAttrObj [name=" + name + ", type=" + type + ", indexed=" + indexed + ", stored=" + stored
				+ ", multiValued=" + multiValued + ", required=" + required + ", defaultValue=" + defaultValue + "]";
	}
	
	
}
