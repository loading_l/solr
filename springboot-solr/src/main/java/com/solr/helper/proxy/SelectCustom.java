package com.solr.helper.proxy;

import org.apache.solr.client.solrj.SolrQuery;

public interface SelectCustom {
	public SolrQuery select();
}
