package com.solr.helper.page;


import java.util.List;
import java.util.Map;

import org.apache.solr.client.solrj.response.QueryResponse;

public class SolrPageHelper{
	private int start;
	private int rows;
	private List<Map<String,Object>> data;
	private long count;//总条数
	private int pageCount;//总页数
	private QueryResponse queryResponse;
	private Map<String, Map<String,Object>> facetData;
	public SolrPageHelper(int start, int rows) {
		this.start = start;
		this.rows = rows;
	}
	public Map<String, Map<String, Object>> getFacetData() {
		return facetData;
	}
	public void setFacetData(Map<String, Map<String, Object>> facetData) {
		this.facetData = facetData;
	}
	public QueryResponse getQueryResponse() {
		return queryResponse;
	}
	public void setQueryResponse(QueryResponse queryResponse) {
		this.queryResponse = queryResponse;
	}
	public int getPageCount() {
		return pageCount;
	}
	public void setPageCount(int totalPage) {
		this.pageCount = totalPage;
	}
	public long getCount() {
		return count;
	}
	public void setCount(long count) {
		this.count = count;
	}
	public int getStart() {
		return start;
	}
	protected  void setStart(int start) {
		this.start = start;
	}
	public int getRows() {
		return rows;
	}
	public List<Map<String,Object>> getData() {
		return data;
	}
	public void setData(List<Map<String,Object>> data) {
		this.data = data;
	}
	@Override
	public String toString() {
		return "SolrPageHelper [start=" + start + ", rows=" + rows + ", data=" + data + ", count="
				+ count + "]";
	}
}
