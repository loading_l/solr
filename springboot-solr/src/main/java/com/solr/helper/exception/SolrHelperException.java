package com.solr.helper.exception;

public class SolrHelperException extends Exception{
	private static final long serialVersionUID = 3502418225437981100L;
	public SolrHelperException(String errMsg) {
		super(errMsg);
	}
}
