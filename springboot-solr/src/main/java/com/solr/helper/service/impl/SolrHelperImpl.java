package com.solr.helper.service.impl;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.ConcurrentUpdateSolrClient;
import org.apache.solr.client.solrj.request.schema.SchemaRequest;
import org.apache.solr.client.solrj.request.schema.SchemaRequest.ReplaceField;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.client.solrj.response.schema.SchemaResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.FacetParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.core.schema.SchemaDefinition;
import org.springframework.data.solr.core.schema.SchemaDefinition.FieldDefinition;
import org.springframework.data.solr.core.schema.SchemaDefinition.SchemaField;
import org.springframework.data.solr.core.schema.SchemaOperations;

import com.solr.config.SolrHelperCluster;
import com.solr.config.SolrHelperConfig;
import com.solr.config.SolrHelperStandalone;
import com.solr.helper.entity.FieldAttrObj;
import com.solr.helper.exception.SolrHelperException;
import com.solr.helper.page.SolrPageHelper;
import com.solr.helper.proxy.SelectCustom;
import com.solr.helper.service.SolrHelper;

import cn.newstrength.wtdf.plugin.result.SuccessTranResult;
import cn.newstrength.wtdf.plugin.result.TranResult;
import cn.newstrength.wtdf.plugin.util.TranUtils;


public class SolrHelperImpl implements SolrHelper,Serializable {
	private static final long serialVersionUID = 5167867686878596972L;
	private static final Logger logger = LoggerFactory.getLogger(SolrHelper.class);
	private static final String FIELD_TYPE_STRING = "string";
	
	private static final String LOG_ID_NOT_NULL = "核心库[{}]: 唯一标识id不能为空";
	
	private static final String LOG_UPDATE_FAILED = "核心库[{}]: 更新数据失败: {}";
	private static final String LOG_UPDATE_SUCCESS = "核心库[{}]: 更新数据成功: {}";
	
	private static final String LOG_DELETE_SUCCESS = "核心库[{}]: 删除数据成功: response={}";
	private static final String LOG_DELETE_FAILED = "核心库[{}]: 删除数据失败: response={}";
	
	private static final String LOG_RESPONSE_UPDATE = "核心库[{}]: 更新数据响应结果: reponse={}";
	private static final String LOG_RESPONSE_DELETE = "核心库[{}]: 删除数据返回结果: response={}";
	
	private SolrClient solrClient;
	private SolrTemplate solrTemplate;
	private ConcurrentUpdateSolrClient concurrentUpdateSolrClient;//solr单机批处理
	private long commitMillis;//软提交时间间隔单位毫秒
	
	public SolrHelperImpl(SolrHelperConfig solrHelperConfig) {
		//集群模式
		if("true".equalsIgnoreCase(solrHelperConfig.getCluster())) {
			SolrHelperCluster solrHelperCluster = solrHelperConfig.getSolrHelperCluster();
			this.solrClient = solrHelperCluster.getSolrClient();
			this.solrTemplate = solrHelperCluster.getSolrTemplate();
		}
		//单机模式
		if("false".equalsIgnoreCase(solrHelperConfig.getCluster())) {
			SolrHelperStandalone solrHelperStandalone = solrHelperConfig.getSolrHelperStandalone();
			this.solrClient = solrHelperStandalone.getSolrClient();
			this.solrTemplate = solrHelperStandalone.getSolrTemplate();
			this.concurrentUpdateSolrClient = solrHelperStandalone.getConcurrentUpdateSolrClient();
		}
		this.commitMillis = solrHelperConfig.getCommitMillis();
	}
	
	/**
	 * author 梁博钧
	 * description 添加域
	 * 2019年4月24日 下午10:36:00
	 * @param coreName 核心库名称
	 * @param FieldAttrObj 域属性
	 * @return 添加成功返回true 添加失败返回false
	 * @throws SolrHelperException 
	 * @throws Exception 
	 */
	public TranResult<Boolean> createField(String coreName, FieldAttrObj filedAttr) throws SolrHelperException{
		//域名称，域类型不能为空
		if(filedAttr.getName()==null || "".equals(filedAttr.getName()) || filedAttr.getType()==null || "".equals(filedAttr.getType())) {
			logger.info("核心库[{}]: 创建域失败，创建域域时，域名称、域类型不能为空或空字符串！",coreName);
			return new SuccessTranResult<Boolean>(false);
		}
		if(existField(coreName,filedAttr.getName()).getData()) {
			logger.info("核心库[{}]: 创建域失败，{} 域已存在！",coreName,filedAttr.getName());
			return new SuccessTranResult<Boolean>(false);
		}
		SchemaOperations schemaOperations = solrTemplate.getSchemaOperations(coreName);//获取操作模式
		Map<String, Object> params = new HashMap<String,Object>();//创建一个域并设置域属性
		params.put("name", filedAttr.getName());
		params.put("type", filedAttr.getType());
		params.put("indexed", filedAttr.isIndexed());
		params.put("stored", filedAttr.isStored());
		params.put("multiValued", filedAttr.isMultiValued());
		params.put("required", filedAttr.isRequired());
		params.put("default", filedAttr.getDefaultValue());
		SchemaField schemaField = FieldDefinition.fromMap(params);//设置域参数
		try {
			schemaOperations.addField(schemaField);//添加域
		} catch (Exception e) {
			throw new SolrHelperException("添加域时发生异常 fieldName="+filedAttr.getName());
		}
		logger.info("核心库[{}]: 创建域成功: {}",coreName,filedAttr);
		return new SuccessTranResult<Boolean>(true);
	}
	
	/**
	 * Author:梁博钧
	 * Description:修改域类型
	 * 2019年6月21日 上午10:24:14
	 * @param coreName
	 * @param fieldAttrObj
	 * @return true成功，false失败
	 * @throws SolrServerException
	 * @throws IOException
	 */
	public TranResult<Boolean> replaceField(String coreName, FieldAttrObj fieldAttrObj) throws SolrServerException, IOException{
		Map<String, Object> fieldAttributes=new HashMap<>();
		fieldAttributes.put("name", fieldAttrObj.getName());
		fieldAttributes.put("type", fieldAttrObj.getType());
		fieldAttributes.put("indexed", fieldAttrObj.isIndexed());
		fieldAttributes.put("stored", fieldAttrObj.isStored());
		fieldAttributes.put("multiValued", fieldAttrObj.isMultiValued());
		fieldAttributes.put("required", fieldAttrObj.isRequired());
		ReplaceField replaceField= new SchemaRequest.ReplaceField(fieldAttributes);
		SchemaResponse.UpdateResponse process = replaceField.process(solrClient,coreName);
		if(0==process.getStatus()){
			logger.info("核心库[{}]: 修改域类型成功 {}",coreName,fieldAttrObj);
			return new SuccessTranResult<Boolean>(true);
		}else{
			logger.info("核心库[{}]: 修改域类型失败 {}",coreName,fieldAttrObj);
			return new SuccessTranResult<Boolean>(false);
		}
	}
	
	/**
	 * 
	 * author 梁博钧
	 * description 删除域
	 * 2019年4月24日 下午11:29:32
	 * @param coreName 核心库名称
	 * @param fieldName 域名称
	 * @return 删除成功 true 删除失败 false
	 */
	public TranResult<Boolean> removeField(String coreName, String fieldName) {
		//判断核心库中域是否存在
		if(!existField(coreName, fieldName).getData()) {
			logger.info("核心库[{}]: 删除域失败，{} 域不存在！",coreName,fieldName);
			return new SuccessTranResult<Boolean>(false);
		}
		SchemaOperations schemaOperations = solrTemplate.getSchemaOperations(coreName);
		schemaOperations.removeField(fieldName);
		logger.info("核心库[{}]: 删除域成功: fieldName={}",coreName,fieldName);
		return new SuccessTranResult<Boolean>(true);
	}
	
	/**
	 * author 梁博钧
	 * description 判断域是否存在
	 * 2019年4月24日 下午10:34:36
	 * @param coreName 核心库名称
	 * @param fieldName 域名称
	 * @return 存在 true 不存在 false
	 */
	public TranResult<Boolean> existField(String coreName ,String fieldName) {
		SchemaOperations schemaOperations = solrTemplate.getSchemaOperations(coreName);//获取操作模式
		SchemaDefinition readSchema = schemaOperations.readSchema();//获取模式定义
		FieldDefinition fieldDefinition = readSchema.getFieldDefinition(fieldName);//获取域定义
		if(fieldDefinition!=null){
			logger.info("核心库[{}]: 查询域 {} 存在",coreName,fieldName);
			return new SuccessTranResult<Boolean>(true);
		}else{
			logger.info("核心库[{}]: 查询域 {} 不存在",coreName,fieldName);
			return new SuccessTranResult<Boolean>(false);
		}
	}
	
	/**
	 * author 梁博钧
	 * description 查询单个域
	 * 2019年4月24日 下午10:34:36
	 * @param coreName 核心库名称
	 * @param fieldName 域名称
	 * @return 域属性对象
	 */
	public TranResult<FieldAttrObj> getField(String coreName ,String fieldName) {
		SchemaOperations schemaOperations = solrTemplate.getSchemaOperations(coreName);//获取操作模式
		SchemaDefinition readSchema = schemaOperations.readSchema();//获取模式定义
		FieldDefinition fieldDefinition = readSchema.getFieldDefinition(fieldName);//获取域定义
		if(fieldDefinition!=null){
			logger.info("核心库[{}]: 查询域 {} 存在",coreName,fieldName);
			FieldAttrObj fao = new FieldAttrObj(fieldDefinition.getName(),fieldDefinition.getType())
			.setIndexed(fieldDefinition.isIndexed()).setStored(fieldDefinition.isStored())
			.setMultiValued(fieldDefinition.isMultiValued()).setRequired(fieldDefinition.isRequired());
			return new SuccessTranResult<FieldAttrObj>(fao);
		}else{
			logger.info("核心库[{}]: 查询域 {} 不存在",coreName,fieldName);
			return new SuccessTranResult<FieldAttrObj>(null);
		}
		
	}
	
	/**
	 * Author：梁博钧
	 * Description：获取核心库中所有域，id, _version_作为默认域不参与获取
	 * 2019年4月25日 下午4:36:10
	 * @param coreName 核心库名称
	 * @return Map<String,Boolean> key=域名称，value=ture
	 */
	public TranResult<Map<String,FieldAttrObj>> getFields(String coreName){
		Map<String, FieldAttrObj> fields = new HashMap<>();
		SchemaOperations schemaOperations = solrTemplate.getSchemaOperations(coreName);//获取操作模式
		SchemaDefinition readSchema = schemaOperations.readSchema();//获取模式定义
		List<FieldDefinition> getFields = readSchema.getFields();//获取域定义
		for (FieldDefinition fieldDefinition : getFields) {
			String fieldName = fieldDefinition.getName();
			//id, _version_作为默认字段不参与获取
			if("id".equals(fieldName) || "_version_".equals(fieldName)){
				continue;
			}
			fields.put(fieldName, new FieldAttrObj(fieldDefinition.getName(),fieldDefinition.getType())
			.setIndexed(fieldDefinition.isIndexed()).setStored(fieldDefinition.isStored())
			.setMultiValued(fieldDefinition.isMultiValued()).setRequired(fieldDefinition.isRequired()));
		}
		logger.info("核心库[{}]: 查询所有域: {}",coreName,fields);
		return  new SuccessTranResult<Map<String,FieldAttrObj>>(fields);
	}
	
	
	/**
	 * author 梁博钧
	 * description 向核心库中添加一条数据，添加数据过程中域不存则在动态创建域
	 * 2019年5月2日 下午5:18:59
	 * @param coreName 核心库名称
	 * @param data Map<String,Object> key=域名称，value=值
	 * @return 添加成功 true 添加失败 false
	 * @throws SolrHelperException 
	 */
	public TranResult<Boolean> updateIncrement(String coreName, Map<String,Object> data) throws SolrHelperException{
		//如果id为空或者空字符串返回false
		if(data.get("id")==null || "".equals(data.get("id"))){
			logger.info(LOG_ID_NOT_NULL,coreName);
			return new SuccessTranResult<Boolean>(false);
		}
		SolrInputDocument docu = new SolrInputDocument();//创建solr文档对象
		//遍历数据,判断域是否存在,不存在则创建
		for (Map.Entry<String, Object> entry : data.entrySet()) {
			String key = entry.getKey();//得到key也就是域名称
			//判断当前域在核心库中是否存在,不存在则创建当前域
			if(!existField(coreName, key).getData()){
				TranResult<Boolean> addField = createField(coreName, new FieldAttrObj(key,FIELD_TYPE_STRING));//创建域
				String status = addField.getData()?"创建成功!":"创建失败!";
				logger.info("核心库[{}]: 添加数据时动态创建域: "+key+":{}",coreName,status);
				if(!addField.getData()) {
					return new SuccessTranResult<Boolean>(false);
				}
			}
			//添加数据
			docu.addField(entry.getKey(), entry.getValue());
		}
		UpdateResponse res = solrTemplate.saveDocument(coreName, docu);//提交数据
		solrTemplate.commit(coreName);
		logger.info(LOG_RESPONSE_UPDATE,coreName,res.getResponse());//获取响应结果
		//判断响应状态
		if(0==res.getStatus()){
			logger.info(LOG_UPDATE_SUCCESS,coreName,data);
			return new SuccessTranResult<Boolean>(true);
		}else{
			logger.info(LOG_UPDATE_FAILED,coreName,data);
			return new SuccessTranResult<Boolean>(false);
		}
	}
	
	
	/**
	 * Author:梁博钧
	 * Description: 全量添加，请保证域存在，添加单条数据
	 * 2019年6月14日 下午3:03:37
	 * @param coreName
	 * @param data
	 * @return
	 */
	public TranResult<Boolean> updateFullDose(String coreName, Map<String,Object> data){
		//如果id为空或者空字符串返回false
		if(data.get("id")==null || "".equals(data.get("id"))){
			logger.info(LOG_ID_NOT_NULL,coreName);
			return new SuccessTranResult<Boolean>(false);
		}
		SolrInputDocument docu = new SolrInputDocument();//创建solr文档对象
		//遍历数据,判断域是否存在,不存在则创建
		for (Map.Entry<String, Object> entry : data.entrySet()) {
			//添加数据
			docu.addField(entry.getKey(), entry.getValue());
		}
		UpdateResponse res = solrTemplate.saveDocument(coreName, docu);//提交数据
		solrTemplate.commit(coreName);
		logger.info(LOG_RESPONSE_UPDATE,coreName,res.getResponse());//获取响应结果
		//判断响应状态
		if(0==res.getStatus()){
			logger.info(LOG_UPDATE_SUCCESS,coreName,data);
			return new SuccessTranResult<Boolean>(true);
		}else{
			logger.info(LOG_UPDATE_FAILED,coreName,data);
			return new SuccessTranResult<Boolean>(false);
		}
	}
	
	
	
	/**
	 * Author:梁博钧
	 * Description: 批量全量添加，请保证域存在，添加多条数据，集群添加最快
	 * 2019年6月14日 下午3:03:37
	 * @param coreName
	 * @param data
	 * @return
	 */
	public TranResult<Boolean> updateBatch(String coreName, List<Map<String,Object>> dataList){
		if(dataList == null || dataList.isEmpty()) {
			logger.info("集合中没有元素 list={}",dataList);
			return new SuccessTranResult<Boolean>(false);
		}
		List<SolrInputDocument> list = new ArrayList<SolrInputDocument>();
		for (Map<String, Object> data : dataList) {
			//如果id为空或者空字符串返回false
			if(data.get("id")==null || "".equals(data.get("id"))){
				logger.info(LOG_ID_NOT_NULL,coreName);
				return new SuccessTranResult<Boolean>(false);
			}
			SolrInputDocument docu = new SolrInputDocument();//创建solr文档对象
			//遍历数据,判断域是否存在,不存在则创建
			for (Map.Entry<String, Object> entry : data.entrySet()) {
				//添加数据
				docu.addField(entry.getKey(), entry.getValue());
			}
			list.add(docu);
		}
		UpdateResponse res = null;
		//如果设置软提交时间进行软提交，未设置进行硬提交
		res = solrTemplate.saveDocuments(coreName, list, Duration.ofMillis(this.commitMillis));
		logger.info(LOG_RESPONSE_UPDATE,coreName,res.getResponse());//获取响应结果
		//判断响应状态
		if(0==res.getStatus()){
			logger.info("核心库[{}]: 添加数据集合成功:  共 {} 条",coreName,list.size());
			return new SuccessTranResult<Boolean>(true);
		}else{
			logger.info("核心库[{}]: 添加数据集合失败:  ",coreName);
			return new SuccessTranResult<Boolean>(false);
		}
	}
	
	/**
	 * Author:梁博钧
	 * Description:批量全量添加，请保证域存在，添加单条数据，仅限单机使用
	 * 2019年6月14日 下午4:24:35
	 * @param coreName
	 * @param data
	 * @return
	 * @throws IOException 
	 * @throws SolrServerException 
	 */
	public TranResult<Boolean> updateBatchToStandalone(String coreName, Map<String,Object> data) throws SolrServerException, IOException{
		//如果id为空或者空字符串返回false
		if(data.get("id")==null || "".equals(data.get("id"))){
			logger.info(LOG_ID_NOT_NULL,coreName);
			return new SuccessTranResult<Boolean>(false);
		}
		SolrInputDocument docu = new SolrInputDocument();//创建solr文档对象
		//遍历数据,判断域是否存在,不存在则创建
		for (Map.Entry<String, Object> entry : data.entrySet()) {
			//添加数据
			docu.addField(entry.getKey(), entry.getValue());
		}
		UpdateResponse res = concurrentUpdateSolrClient.add(coreName, docu, (int)this.commitMillis);//提交数据
		logger.info(LOG_RESPONSE_UPDATE,coreName,res.getResponse());//获取响应结果
		//判断响应状态
		if(0==res.getStatus()){
			logger.info(LOG_UPDATE_SUCCESS,coreName,data);
			return new SuccessTranResult<Boolean>(true);
		}else{
			logger.info(LOG_UPDATE_FAILED,coreName,data);
			return new SuccessTranResult<Boolean>(false);
		}
	}
	
	/**
	 * Author:梁博钧
	 * Description:批量全量添加，请保证域存在，添加多条数据，仅限单机使用，添加最快
	 * 2019年6月14日 下午4:24:35
	 * @param coreName
	 * @param data
	 * @return
	 * @throws IOException 
	 * @throws SolrServerException 
	 */
	public TranResult<Boolean> updateBatchToStandalone(String coreName, List<Map<String,Object>> dataList) throws SolrServerException, IOException{
		if(dataList == null || dataList.isEmpty()) {
			logger.info("集合中没有元素 list={}",dataList);
			return new SuccessTranResult<Boolean>(false);
		}
		List<SolrInputDocument> list = new ArrayList<SolrInputDocument>();
		for (Map<String, Object> data : dataList) {
			//如果id为空或者空字符串返回false
			if(data.get("id")==null || "".equals(data.get("id"))){
				logger.info(LOG_ID_NOT_NULL,coreName);
				return new SuccessTranResult<Boolean>(false);
			}
			SolrInputDocument docu = new SolrInputDocument();//创建solr文档对象
			//遍历数据,判断域是否存在,不存在则创建
			for (Map.Entry<String, Object> entry : data.entrySet()) {
				//添加数据
				docu.addField(entry.getKey(), entry.getValue());
			}
			list.add(docu);
		}
		UpdateResponse res = null;
		//如果设置软提交时间进行软提交，未设置进行硬提交
		res = concurrentUpdateSolrClient.add(coreName, list, (int)this.commitMillis);
		logger.info(LOG_RESPONSE_UPDATE,coreName,res.getResponse());//获取响应结果
		//判断响应状态
		if(0==res.getStatus()){
			logger.info("核心库[{}]: 添加数据集合成功:  共 {} 条",coreName,list.size());
			return new SuccessTranResult<Boolean>(true);
		}else{
			logger.info("核心库[{}]: 添加数据集合失败:  ",coreName);
			return new SuccessTranResult<Boolean>(false);
		}
	}
	
	/**
	 * author 梁博钧
	 * description 单个id删除数据
	 * 2019年5月3日 上午9:53:17
	 * @param coreName 核心库名称
	 * @param id 
	 * @return 删除成功 true 删除失败 false
	 * @throws IOException 
	 * @throws SolrServerException 
	 */
	public TranResult<Boolean> deleteDataById(String coreName, String id) throws SolrServerException, IOException{
		if(id==null || "".equals(id)) {
			logger.info(LOG_ID_NOT_NULL,coreName);
		}
		UpdateResponse res = solrClient.deleteById(coreName, id);
		solrClient.commit(coreName);
		solrClient.optimize(coreName);
        logger.info(LOG_RESPONSE_DELETE,coreName,res.getResponse());
        if(0==res.getStatus()){
        	logger.info(LOG_DELETE_SUCCESS,coreName,res.getResponse());
        	return new SuccessTranResult<Boolean>(true);
        }else{
        	logger.info(LOG_DELETE_FAILED,coreName,res.getResponse());
        	return new SuccessTranResult<Boolean>(false);
        }
	}
	
	/**
	 * author 梁博钧
	 * description 多个id删除数据
	 * 2019年5月3日 上午9:53:17
	 * @param coreName 核心库名称
	 * @param id 
	 * @return 删除成功 true 删除失败 false
	 * @throws IOException 
	 * @throws SolrServerException 
	 */
	public TranResult<Boolean> deleteDataByIds(String coreName, List<String> ids) throws SolrServerException, IOException{
		if(ids==null || ids.isEmpty()) {
			logger.info(LOG_ID_NOT_NULL,coreName);
		}
		UpdateResponse res = solrClient.deleteById(coreName, ids);
		solrClient.commit(coreName);
		solrClient.optimize(coreName);//回收磁盘
        logger.info(LOG_RESPONSE_DELETE,coreName,res.getResponse());
        if(0==res.getStatus()){
        	logger.info(LOG_DELETE_SUCCESS,coreName,res.getResponse());
        	return new SuccessTranResult<Boolean>(true);
        }else{
        	logger.info(LOG_DELETE_FAILED,coreName,res.getResponse());
        	return new SuccessTranResult<Boolean>(false);
        }
	}
	
	
	/**
	 * author 梁博钧
	 * description 自定义普通查询
	 * 2019年5月3日 下午12:25:31
	 * @param coreName
	 * @param solrPageHelper
	 * @param SelectCustom 实现SElectCustom中select方法，并由本方法执行select方法
	 * @returnTranResult<SolrPageHelper<T>> 返回分页信息及查询给定类型的结果集
	 * @throws SolrServerException
	 * @throws IOException
	 */
	public TranResult<SolrPageHelper> queryForListPage(String coreName , SolrPageHelper solrPageHelper, SelectCustom selectCustom) throws SolrServerException, IOException{
		SolrQuery query = selectCustom.select();//执行代理类已实现的select方法
		query.setStart((solrPageHelper.getStart()-1)*solrPageHelper.getRows());//设置页码
		query.setRows(solrPageHelper.getRows());//设置每页显示行数
        QueryResponse res = solrClient.query(coreName, query);//获取响应结果
        solrPageHelper.setQueryResponse(res);
        return getSelectResult(solrPageHelper, res);
	}
	
	
	
	
	/**
	 * author 梁博钧
	 * description 处理普通查询结果集
	 * 2019年5月3日 下午12:22:48
	 * @param solrPageHelper
	 * @param results
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private TranResult<SolrPageHelper> getSelectResult(SolrPageHelper solrPageHelper, QueryResponse res) {
		SolrDocumentList results = res.getResults();//获取结果集
		solrPageHelper.setCount(results.getNumFound());//设置总条数
		//设置总页数
		int pageCount = div(results.getNumFound(),solrPageHelper.getRows());//做精确除法，返回整数部分
		if(results.getNumFound()%solrPageHelper.getRows()!=0) {
			pageCount += 1;
		}
		solrPageHelper.setPageCount(pageCount);
		List<Map<String,Object>> resultList = new ArrayList<>();
        for (SolrDocument solrDocument : results) {//遍历结果集
        	String jsonString = TranUtils.toJson(solrDocument);//将结果集转换为json字符串
        	//将json字符串转换为具体对象
        	resultList.add(TranUtils.jsonToBean(jsonString,Map.class));//将json字符串转换为具体对象添加进返回结果中
		}
        solrPageHelper.setData(resultList);//封装返回结果
        return new SuccessTranResult<SolrPageHelper>(solrPageHelper);
	}
	
	/**
	 * author 梁博钧
	 * description 自定义Facet查询
	 * 2019年5月3日 下午12:25:31
	 * @param coreName
	 * @param solrPageHelper
	 * @param SelectFacetCustom 实现SelectFacetCustom中select方法，并由本方法执行select方法
	 * @returnTranResult<SolrPageHelper<T>> 返回分页信息及查询给定类型的结果集
	 * @throws SolrServerException
	 * @throws IOException
	 */
	public TranResult<SolrPageHelper> selectFacetCustom(String coreName , SolrPageHelper solrPageHelper, SelectCustom selectFacetCustom) throws SolrServerException, IOException{
		SolrQuery query = selectFacetCustom.select();//执行内部类已实现的select方法
		query.set(FacetParams.FACET_OFFSET,(solrPageHelper.getStart()-1)*solrPageHelper.getRows());//设置页码
		query.setFacetLimit(solrPageHelper.getRows());// 限制facet返回的数量
		query.setRows(0);//禁止普通查询返回结果
        QueryResponse res = solrClient.query(coreName, query);//获取响应结果
        solrPageHelper.setQueryResponse(res);
        return getSelectFacetResult(solrPageHelper, res);
	}
	/**
	 * Author:梁博钧
	 * Description: 处理Facet查询结果集
	 * 2019年5月6日 上午9:59:41
	 * @param solrPageHelper
	 * @param res SolrQuery查询响应结果
	 * @return
	 */
	private TranResult<SolrPageHelper> getSelectFacetResult(SolrPageHelper solrPageHelper, QueryResponse res) {
		Map<String, Map<String,Object>> fieldsData = new HashMap<String, Map<String,Object>>();
		List<FacetField> facetFields = res.getFacetFields();//获取facet查询结果集
		for (FacetField facetField : facetFields) {
			String name = facetField.getName();//查询的域名称
			Map<String,Object> valuesMap = new HashMap<String,Object>();
			List<Count> values = facetField.getValues();//对应域的值分组统计结果
			for (Count count : values) {
				valuesMap.put(count.getName(), count.getCount());
			}
			fieldsData.put(name, valuesMap);
		}
		solrPageHelper.setFacetData(fieldsData);
		return new SuccessTranResult<SolrPageHelper>(solrPageHelper);
	}
	/**
	 * author 梁博钧
	 * description 判断一个值的数据基本类型
	 * 2019年5月2日 下午5:41:14
	 * @param value 值
	 * @return 数据类型
	 */
	public String getDataType(Object value) {
		String identityToString = ObjectUtils.identityToString(value);
		if(identityToString.indexOf("String")!=-1) {
			return FIELD_TYPE_STRING;
		}else if(identityToString.indexOf("Integer")!=-1) {
			return "long";
		}else if(identityToString.indexOf("Long")!=-1) {
			return "long";
		}else if(identityToString.indexOf("Double")!=-1){
			return "double";
		}else if(identityToString.indexOf("Date")!=-1){
			return "date";
		}else {
			return FIELD_TYPE_STRING;
		}
	}
	
	/**
	 * author 梁博钧
	 * description 精确除法，舍弃小数部分，只保留整数
	 * 2019年6月6日 下午3:36:54
	 * @param
	 * @param t1
	 * @param t2
	 * @return
	 */
	private <T extends Number> int div(T t1, T t2) {
		BigDecimal b1 = BigDecimal.valueOf(t1.doubleValue());
		BigDecimal b2 = BigDecimal.valueOf(t2.doubleValue());
		double doubleValue = b1.divide(b2, 2, BigDecimal.ROUND_HALF_UP).doubleValue();
		String valueOf = String.valueOf(doubleValue);
		return Integer.parseInt(valueOf.substring(0, valueOf.indexOf('.')));
	}

}
