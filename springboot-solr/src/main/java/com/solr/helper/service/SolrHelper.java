package com.solr.helper.service;



import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.solr.client.solrj.SolrServerException;

import com.solr.helper.entity.FieldAttrObj;
import com.solr.helper.exception.SolrHelperException;
import com.solr.helper.page.SolrPageHelper;
import com.solr.helper.proxy.SelectCustom;

import cn.newstrength.wtdf.plugin.result.TranResult;


public interface SolrHelper{
	
	/**
	 * author 梁博钧
	 * description 添加域
	 * 2019年4月24日 下午10:36:00
	 * @param coreName 核心库名称
	 * @param FieldAttrObj 域属性
	 * @return 添加成功返回true 添加失败返回false
	 * @throws SolrHelperException 
	 * @throws Exception 
	 */
	public TranResult<Boolean> createField(String coreName, FieldAttrObj filedAttr) throws SolrHelperException;
	
	/**
	 * Author:梁博钧
	 * Description:修改域类型
	 * 2019年6月21日 上午10:24:14
	 * @param coreName
	 * @param fieldAttrObj
	 * @return true成功，false失败
	 * @throws SolrServerException
	 * @throws IOException
	 */
	public TranResult<Boolean> replaceField(String coreName, FieldAttrObj fieldAttrObj) throws SolrServerException, IOException;
	
	/**
	 * 
	 * author 梁博钧
	 * description 删除域
	 * 2019年4月24日 下午11:29:32
	 * @param coreName 核心库名称
	 * @param fieldName 域名称
	 * @return 删除成功 true 删除失败 false
	 */
	public TranResult<Boolean> removeField(String coreName, String fieldName);
	
	/**
	 * author 梁博钧
	 * description 判断域是否存在
	 * 2019年4月24日 下午10:34:36
	 * @param coreName 核心库名称
	 * @param fieldName 域名称
	 * @return 存在 true 不存在 false
	 */
	public TranResult<Boolean> existField(String coreName ,String fieldName);
	
	/**
	 * author 梁博钧
	 * description 查询单个域
	 * 2019年4月24日 下午10:34:36
	 * @param coreName 核心库名称
	 * @param fieldName 域名称
	 * @return 域属性对象
	 */
	public TranResult<FieldAttrObj> getField(String coreName ,String fieldName);
	
	/**
	 * Author：梁博钧
	 * Description：获取核心库中所有域，id, _version_作为默认域不参与获取
	 * 2019年4月25日 下午4:36:10
	 * @param coreName 核心库名称
	 * @return Map<String,Boolean> key=域名称，value=ture
	 */
	public TranResult<Map<String,FieldAttrObj>> getFields(String coreName);
	
	
	/**
	 * author 梁博钧
	 * description 向核心库中添加一条数据，添加数据过程中域不存则在动态创建域
	 * 2019年5月2日 下午5:18:59
	 * @param coreName 核心库名称
	 * @param data Map<String,Object> key=域名称，value=值
	 * @return 添加成功 true 添加失败 false
	 * @throws SolrHelperException 
	 */
	public TranResult<Boolean> updateIncrement(String coreName, Map<String,Object> data) throws SolrHelperException;
	
	
	/**
	 * Author:梁博钧
	 * Description: 全量添加，请保证域存在，添加单条数据
	 * 2019年6月14日 下午3:03:37
	 * @param coreName
	 * @param data
	 * @return
	 */
	public TranResult<Boolean> updateFullDose(String coreName, Map<String,Object> data);
	
	
	
	/**
	 * Author:梁博钧
	 * Description: 批量全量添加，请保证域存在，添加多条数据，集群添加最快
	 * 2019年6月14日 下午3:03:37
	 * @param coreName
	 * @param data
	 * @return
	 */
	public TranResult<Boolean> updateBatch(String coreName, List<Map<String,Object>> dataList);
	
	/**
	 * Author:梁博钧
	 * Description:批量全量添加，请保证域存在，添加单条数据，仅限单机使用
	 * 2019年6月14日 下午4:24:35
	 * @param coreName
	 * @param data
	 * @return
	 * @throws IOException 
	 * @throws SolrServerException 
	 */
	public TranResult<Boolean> updateBatchToStandalone(String coreName, Map<String,Object> data) throws SolrServerException, IOException;
	
	/**
	 * Author:梁博钧
	 * Description:批量全量添加，请保证域存在，添加多条数据，仅限单机使用，添加最快
	 * 2019年6月14日 下午4:24:35
	 * @param coreName
	 * @param data
	 * @return
	 * @throws IOException 
	 * @throws SolrServerException 
	 */
	public TranResult<Boolean> updateBatchToStandalone(String coreName, List<Map<String,Object>> dataList) throws SolrServerException, IOException;
	
	/**
	 * author 梁博钧
	 * description 单个id删除数据
	 * 2019年5月3日 上午9:53:17
	 * @param coreName 核心库名称
	 * @param id 
	 * @return 删除成功 true 删除失败 false
	 * @throws IOException 
	 * @throws SolrServerException 
	 */
	public TranResult<Boolean> deleteDataById(String coreName, String id) throws SolrServerException, IOException;
	
	/**
	 * author 梁博钧
	 * description 多个id删除数据
	 * 2019年5月3日 上午9:53:17
	 * @param coreName 核心库名称
	 * @param id 
	 * @return 删除成功 true 删除失败 false
	 * @throws IOException 
	 * @throws SolrServerException 
	 */
	public TranResult<Boolean> deleteDataByIds(String coreName, List<String> ids) throws SolrServerException, IOException;
	
	
	/**
	 * author 梁博钧
	 * description 自定义普通查询
	 * 2019年5月3日 下午12:25:31
	 * @param coreName
	 * @param solrPageHelper
	 * @param SelectCustom 实现SElectCustom中select方法，并由本方法执行select方法
	 * @return <T>TranResult<SolrPageHelper<T>> 返回分页信息及查询给定类型的结果集
	 * @throws SolrServerException
	 * @throws IOException
	 */
	public TranResult<SolrPageHelper> queryForListPage(String coreName , SolrPageHelper solrPageHelper, SelectCustom selectCustom) throws SolrServerException, IOException;
	
	/**
	 * author 梁博钧
	 * description 自定义Facet查询
	 * 2019年5月3日 下午12:25:31
	 * @param coreName
	 * @param solrPageHelper
	 * @param SelectFacetCustom 实现SelectFacetCustom中select方法，并由本方法执行select方法
	 * @return <T>TranResult<SolrPageHelper<T>> 返回分页信息及查询给定类型的结果集
	 * @throws SolrServerException
	 * @throws IOException
	 */
	public TranResult<SolrPageHelper> selectFacetCustom(String coreName , SolrPageHelper solrPageHelper, SelectCustom selectFacetCustom) throws SolrServerException, IOException;
	
}
